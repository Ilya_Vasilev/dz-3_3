package com;

import com.task1.Bat;
import com.task1.Dolphin;
import com.task1.Eagle;
import com.task1.GoldFish;
import com.task2.BestCarpenterEver;
import com.task2.Stool;
import com.task2.Table;
import com.task2.Thing;
import com.task4.Competition;

public class Main {

    public static void main(String[] args) {
        // 1 задание
        Bat avrora = new Bat();
        avrora.eat("Аврора");
        avrora.sleep("Аврора");
        avrora.wayOfBirth();
        avrora.movement();
        System.out.println();

        Dolphin appa = new Dolphin();
        appa.eat("Аппа");
        appa.sleep("Аппа");
        appa.wayOfBirth();
        appa.movement();
        System.out.println();

        GoldFish sue = new GoldFish();
        sue.eat("Сью");
        sue.sleep("Cью");
        sue.wayOfBirth();
        sue.movement();
        System.out.println();

        Eagle momo = new Eagle();
        momo.eat("Момо");
        momo.sleep("Момо");
        momo.wayOfBirth();
        momo.movement();
        System.out.println();

        // 2 задание
        Thing thing = new Thing();
        Thing thing1 = new Stool();
        Thing thing2 = new Table();

        System.out.println(BestCarpenterEver.stoolRepair(thing));
        System.out.println(BestCarpenterEver.stoolRepair(thing1));
        System.out.println(BestCarpenterEver.stoolRepair(thing2));
        System.out.println();


        // 4 задание
        Competition competition = new Competition();
        // выведем турнирную таблицу всех участников их питомцев с оценками
        competition.settingValues();
        // выведем турнирную табицу всех участников со средний баллом
        // выведем победителей
        competition.averageMark();



    }

}

