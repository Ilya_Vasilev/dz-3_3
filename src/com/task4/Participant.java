package com.task4;

public class Participant {
   private String [] participantName;

   public Participant(){}

    public String[] getParticipant() {
        return participantName;
    }

    public void setParticipant(String[] participantName) {
        this.participantName = participantName;
    }

}
