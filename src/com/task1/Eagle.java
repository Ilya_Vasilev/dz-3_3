package com.task1;

public class Eagle extends Animal{
    public Eagle(){

    }
    @Override
    public void wayOfBirth(){
        System.out.println("Орел откладывает яйца");
    }

    @Override
    public void movement() {
        System.out.println("Скорость орла достигает до 320 км/ч");
    }

}
