package com.task1;

public class Dolphin extends Animal{
    public Dolphin(){
    }
    @Override
    public void wayOfBirth(){
        System.out.println("Дельфин рождается как живородящее");
    }

    @Override
    public void movement() {
        System.out.println("Скорость с которой плывет дельфин перед передней частью корабля может достигать до 65 км/ч");

    }
}
