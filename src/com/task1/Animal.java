package com.task1;

public abstract class Animal {

    public final void eat(String str) {
        System.out.println(str + " кушает, не мешайте!");
    }

    public final void sleep(String str) {
        System.out.println("Тсссс " + str + " спит");
    }
    public abstract void wayOfBirth();

    public abstract void movement();
}
